# Instructions for API-excercise

### Build And Bring up Docker images
```
docker-compose build
docker-compose up
```

### Initialize db and import generated csv data
```
docker-compose exec server python manage.py create_db
docker-compose exec server python manage.py import_students_db
```

### Client
React Frontend should be running on http://localhost:8080

### To Do
1. Production dockerfiles and configs
2. Validate React Forms
3. Unit Tests
4. Auto-deployment
