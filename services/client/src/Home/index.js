import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';

import APIClient from '../apiClient'
import Students from "../Student/students"

const styles = theme => ({
    root: {
      flexGrow: 1,
      marginTop: 30
    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
});

class Home extends React.Component {

    state = {
        value: 0,
        students: []
    };

    async componentDidMount() {
        this.apiClient = new APIClient();
        this.updateFromApi();
    }

    updateFromApi() {
        this.apiClient.getStudents().then((data) =>
            this.setState({...this.state, students: data})
        );
    }

    isStudent = student => this.state.students.find(s => s.id === student.id)
    onEditStudent = (student) => {
        this.updateBackendEdit(student);
    }

    onDeleteStudent = (student) => {
        this.updateBackendDelete(student);
    }

    updateBackendEdit = (student) => {
        if (this.isStudent(student)) {
            this.apiClient.putStudent(student, student.id);
        } else {
            this.apiClient.createStudent(student);
        }
        this.updateStateEdit(student);    
    }

    updateBackendDelete = (student) => {
        if (this.isStudent(student)) {
            this.apiClient.deleteStudent(student.id);
        }
        this.updateStateDelete(student);
    }

    updateStateEdit = (student) => {
        if (this.isStudent(student)) {
            let updated_students = [...this.state.students];
            let index = updated_students.findIndex(s => s.id === student.id);
            updated_students[index] = {...updated_students[index], student};
            this.setState({ students: updated_students });
        } else {
            this.setState({
                ...this.state,
                students: [student, ...this.state.students]
            })
        }
    }

    updateStateDelete = (student) => {
        if (this.isStudent(student)) {
            var newState = this.state.students.filter(s => s.id !== student.id );
            this.setState({students: newState })
        }
    }

    handleTabChange = (event, value) => {
        this.setState({ value });
    };
     
    handleTabChangeIndex = index => {
        this.setState({ value: index });
    };

    renderAddStudent = () => {
        var emptyStudent = true;
        return (
        <Grid item xs={12} md={3}>
            <Students onEditStudent={this.onEditStudent} onDeleteStudent={this.onDeleteStudent} emptyStudent={emptyStudent}/>
        </Grid>
        );
    }

    renderAllStudents = (students) => {
        if (!students) { return [] }
        var emptyStudent = false;
        return students.map((student) => {
            return (
            <Grid item xs={12} md={3} key={student.id}>
                <Students onEditStudent={this.onEditStudent} onDeleteStudent={this.onDeleteStudent} emptyStudent={emptyStudent} student={student} />
            </Grid>
            );
        })
    }

    render() {
        return (
            <div className={styles.root}>
                <Tabs
                    value={this.state.value}
                    onChange={this.handleTabChange}
                    indicatorColor="primary"
                    textColor="primary"
                    fullwidth="true"
                    >
                    <Tab label="Add Student" />    
                    <Tab label="Students" />
                </Tabs>
                <SwipeableViews
                    axis={'x-reverse'}
                    index={this.state.value}
                >
                    <Grid container spacing={4} style={{padding: '20px 0'}}>
                        { this.renderAddStudent()}
                    </Grid>
                    <Grid container spacing={4} style={{padding: '20px 0'}}>
                        { this.renderAllStudents(this.state.students) }
                    </Grid>
                </SwipeableViews>
                <header className="App-header">
                <p>
                    API-Excercise Home
                </p>
                </header>
            </div>
        )
    }
}

export default Home;