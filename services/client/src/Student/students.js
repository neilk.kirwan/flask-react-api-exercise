import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    card: {
      maxWidth: 800,
      title: '12px',
    },
    media: {
      height: 20,
      paddingTop: '56.25%', // 16:9
    },
    actions: {
      display: 'flex',
    },
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '80%',
        },
    },
});

class Students extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            class_id: '',
            sex: '',
            age: '',
            siblings: '',
            gpa: '',
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleClassChange = this.handleClassChange.bind(this);
        this.handleSexChange = this.handleSexChange.bind(this);
        this.handleAgeChange = this.handleAgeChange.bind(this);
        this.handleSiblingsChange = this.handleSiblingsChange.bind(this);
        this.handleGPAChange = this.handleGPAChange.bind(this);
    }

    handleNameChange(event) {
        this.setState({ name: event.target.value });
      }
      
    handleClassChange(event) {
        this.setState({ class: event.target.value });
    }

    handleSexChange(event) {
        this.setState({ sex: event.target.value });
    }

    handleAgeChange(event) {
        this.setState({ age: event.target.value });
    }

    handleSiblingsChange(event) {
        this.setState({ siblings: event.target.value });
    }

    handleGPAChange(event) {
        this.setState({ gpa: event.target.value });
    }

    handleClickDelete = (event) =>  {
        this.props.onDeleteStudent(this.state)
    }

    handleClickSave = (event) =>  {
        if(this.props.student !== undefined) {
            this.updateStateObject(this.props.student) ;
        }
        this.props.onEditStudent(this.state)
    }

    updateStateObject = (student) => {
        let newState = this.state
        newState.id = student.id;
        newState.name = student.name;
        newState.class_id = student.class_id;
        newState.sex = student.sex;
        newState.age = student.age;
        newState.siblings = student.siblings;
        newState.gpa = student.gpa;
        this.setState({ state: newState })  
    }

    render() {
        const { classes } = this.props;
        return (
            <Card className={classes.card}>
                <CardContent>
                <form className={classes.root} autoComplete="off">
                    <TextField required id="standard-required" label="Name" onChange={this.handleNameChange} defaultValue={this.props.emptyStudent ? '' : this.props.student.name} />
                    <TextField required id="standard-required" label="Class" onChange={this.handleClassChange}  type="number" inputProps={{ min: "0", max: "100", step: "1" }} defaultValue={this.props.emptyStudent ? '' : this.props.student.class_id} />
                    <TextField required id="standard-required" label="Sex" onChange={this.handleSexChange} defaultValue={this.props.emptyStudent ? '' : this.props.student.sex} />
                    <TextField required id="standard-required" label="Age" onChange={this.handleAgeChange} type="number" inputProps={{ min: "0", max: "110", step: "1" }} defaultValue={this.props.emptyStudent ? '' : this.props.student.age} />
                    <TextField required id="standard-required" label="Siblings" onChange={this.handleSiblingsChange}  type="number" inputProps={{ min: "0", max: "20", step: "1" }} defaultValue={this.props.emptyStudent ? '' : this.props.student.siblings} />
                    <TextField required id="standard-required" label="GPA" onChange={this.handleGPAChange} type="number" inputProps={{ min: "0.0", max: "4.2", step: "0.1" }} defaultValue={this.props.emptyStudent ? '' : this.props.student.gpa} />
                </form>
                </CardContent>
                <CardActions className={classes.actions} disableactionspacing="true">
                    <Button variant="contained" onClick={this.handleClickSave} color="primary" size="small" className={classes.button} startIcon={<SaveIcon />}>
                        Save
                    </Button>
                    {!this.props.emptyStudent &&
                        <Button variant="contained" onClick={this.handleClickDelete} color="secondary" size="small" className={classes.button} startIcon={<DeleteIcon />}>
                            Delete
                        </Button>
                    }   
                </CardActions>
            </Card>
        );
    }
}

export default withStyles(styles)(Students);