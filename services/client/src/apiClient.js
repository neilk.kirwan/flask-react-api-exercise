import axios from 'axios';

const BASE_URI = 'http://localhost:5000';

const client = axios.create({
 baseURL: BASE_URI,
 json: true
});

class APIClient {

 getStudents() {
   return this.perform('get', '/api/students');
 }

 createStudent(student) {
    return this.perform('post', '/api/student', student);
 }

 getStudent(uuid) {
    return this.perform('get', `/api/student/${uuid}`);
 }

 putStudent(student, uuid) {
    return this.perform('put', `/api/student/${uuid}`, student);
 }

 deleteStudent(uuid) {
   return this.perform('delete', `/api/student/${uuid}`);
 }
 
 async perform (method, resource, data) {
   return client({
     method,
     url: resource,
     data,
   }).then(resp => {
     return resp.data ? resp.data : [];
   })
 }
}

export default APIClient;