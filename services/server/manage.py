import csv
from flask.cli import FlaskGroup

from project import create_app, db
from project.api.models import Student

app = create_app()
cli = FlaskGroup(create_app=create_app)

@cli.command('init_and_populate_db')
def init_and_populate_db():
    create_db()
    import_students_from_csv_db()

@cli.command('create_db')
def create_db():
    db.create_all()
    db.session.commit()

@cli.command('import_students_db')
def import_students_from_csv_db():
    csv_data = csv.DictReader(open('student.csv', 'r'), delimiter=',')
    for row in csv_data:
        student_values = {}
        for key,value in row.items():
            student_values[key] = value
        student = Student(id=student_values['id'], class_id=student_values['class_id'], name=student_values['name'], sex=student_values['sex'], age=student_values['age'], 
        siblings=student_values['siblings'],gpa=student_values['gpa'])
        db.session.add(student)
        db.session.commit()

if __name__ == '__main__':
    cli()