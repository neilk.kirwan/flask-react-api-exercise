import os

class Config(object):
    POSTGRES_USER = os.environ.get('POSTGRES_USER')
    if not POSTGRES_USER:
        POSTGRES_USER = 'postgres'
    POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
    if not POSTGRES_PASSWORD:
        POSTGRES_PASSWORD = 'secret'
    SQLALCHEMY_DATABASE_URI = f'postgres://{POSTGRES_USER}:{POSTGRES_PASSWORD}@db:5432/student'
    SQLALCHEMY_TRACK_MODIFICATIONS = False