from flask import Flask
from flask.cli import FlaskGroup
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy 

from .config import Config

db = SQLAlchemy()
migrate = Migrate()

def create_app(script_info=None):
    app = Flask(__name__)
    CORS(app)
    app.config.from_object(Config)

    db.init_app(app)

    migrate.init_app(db,app)

    from .api.students import students_blueprint
    app.register_blueprint(students_blueprint)

    @app.shell_context_processor
    def shell_context():
        return {'app': app, 'db': db}

    return app