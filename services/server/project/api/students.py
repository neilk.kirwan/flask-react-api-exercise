from flask import Blueprint, request, jsonify
from uuid import uuid4

from project import db
from .models import Student

students_blueprint = Blueprint('students', __name__, url_prefix='/api/')

@students_blueprint.route('/students', methods=['GET'])
def api_get_students():
    if request.method == 'GET':
        return jsonify(Student.query.all())        

@students_blueprint.route('/student', methods=['POST'])
def api_post_one_student():
    if request.method == 'POST':
        student = get_student_data_from_request_json(request.get_json())
        db.session.add(student)
        db.session.commit()
        return jsonify(student)

@students_blueprint.route('/student/<uuid>', methods=['GET', 'PUT', 'DELETE'])
def api_one_student(uuid):
    student_object = Student.query.filter_by(id=uuid).first()
    if request.method == 'PUT':
        student = get_student_data_from_request_json(request.get_json())
        student_object.id = student.id
        student_object.class_id = student.class_id
        student_object.name = student.name
        student_object.sex = student.sex
        student_object.age = student.age
        student_object.siblings = student.siblings
        student_object.gpa = student.gpa
        db.session.commit()
        return jsonify(student_object)
    if request.method == 'DELETE':
        db.session.delete(student_object)
        db.session.commit()
        return jsonify("Student: " + student_object.id + " deleted")
    if request.method == 'GET':
        return jsonify(student_object)          

def get_student_data_from_request_json(student_data):
    if(student_data.get("id")):
        id = student_data.get("id")
    else:
        id = str(uuid4())
    class_id = student_data.get("class_id")
    name = student_data.get("name")
    sex = student_data.get("sex")
    age = student_data.get("age")
    siblings = student_data.get("siblings")
    gpa = student_data.get("gpa")
    student = Student(id=id,class_id=class_id, name=name, sex=sex, age=age, siblings=siblings, gpa=gpa)
    return student