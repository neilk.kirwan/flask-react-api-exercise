from dataclasses import dataclass
from uuid import uuid4

from project import db

@dataclass
class Student(db.Model):
    id: str
    class_id : int
    name: str
    sex: str
    age: int
    siblings: int = 0
    gpa: int

    id = db.Column(db.String, primary_key=True, default=uuid4)
    class_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(), nullable=False)
    sex = db.Column(db.String, nullable=False)
    age = db.Column(db.Integer, nullable=False)
    siblings = db.Column(db.Integer, nullable=False)
    gpa = db.Column(db.Float, nullable=False)

    def __init__(self, id, class_id, name, sex, age, siblings, gpa):
        self.id = id
        self.class_id = class_id
        self.name = name
        self.sex = sex
        self.age = age
        self.siblings = siblings
        self.gpa = gpa